# podman

Podman learning & notes

# Podman

## Synopsis

The goal of this project is generally learning to use podman in order to move some production off of docker.  More specifically looking at running rootless containers, and learning automation & configuration tools around podman.

For a general test case our storage server is an opportune testing environment.  Container workloads on the host are intermittent and non-critical.  Typically spin up containers for some file administration or atypical processing workloads.  For example recently deployed a temporary folding_at_home workload to utitlize the unused processing power of the host.  The goal is containers defined in yaml files, start/stop functionality via cli and gui, and easy method of updating image versions without cumbersome ```rm``` and ```run``` cli commands.

In general would like to be able to replicate the control & automation I currently have via docker-compose and cockpit-docker functionality.  In reading through a number of blogs/articles this should be achievable.  Key decisions will need to be made 0n ```podman play kube``` vs ```podman-compose``` and ```podman-cockpit``` vs systemd service files.  Currently preferences are ```podman play kube``` due to overlap with k8s learning initiative and systemd service files since their administration is already part of the cockpit ui.

## Action List

  * Deploy podman on Ubuntu server
  * write podman create/run statements, test image OCI compliance
  * use podman to generate k8s yaml file
  * use podman to generate systemd service file
  * determine if ```podman-compose``` is viable
  * determine if ```podman-cockpit``` is currenty viable

## General Learning & Testing

### Ubuntu Installation

Podman is not currently available in the Ubuntu ```apt``` repositories.  A .deb is available from and maintained by the opensuse kubic procect, via a PPA.  Have successfully test installed on a host using the cli commands via ansible, and deployed a container.  Currently developing and optimizing ansible roles to automate the PPA addition and podman installation.

https://podman.io/getting-started/installation.html
https://computingforgeeks.com/how-to-install-podman-on-ubuntu/

I also see that podman and conmon are nativly available in the Solus eopkg repository.  

For Ubuntu repository, looks like it is comming in 20.10. 
https://launchpad.net/ubuntu/+source/libpod
So may be able to install from that repository.

Podman-cockpit is not nativly available on Ubuntu.  In limited research (early Aug 2020) I see some talk of future packaging it for debian repositories, so in theory then a build could be used on Ubuntu.  But that is in the (near?) future, and the build would be ahead of our Cockpit version 215 we get from our LTS repository, so could be an issue.  Building from source may be an option, but well outside my experience/skill.  Current thinking is that podman-cockpit not needed for storage server, the systemd services would be usable, maybe even preferred. And for future on Production we are looking at shifting from docker to k8s, so podman not needed in that environment.  Still the systemd route doesn't provide for image mgt, just start/restart/stop.  At this time though cockpit-docker doesn't do image mgt correctly either, so not missing there.  In production I spin up portainer to remove old images, but for this case would be just as easy to manage from cli.

### Podman Run

So took a simple ```docker run``` statement and modified to ```podman run```.  Everything worked.

### Generate kube yaml

In researching how to define containers in a yaml file to replicate ```docker-compose``` functionality, came upon this blog, link https://mkdev.me/en/posts/dockerless-part-3-moving-development-environment-to-containers-with-podman .  Here I got my first exposure to ```podman generate kube foo > foo.yaml```, which generates a k8s compatible yaml file.  Ok, so now we see a tool to test containers in podman, and then deploying in k8s.  (Will be a good tool for learning k8s yaml for my microk8s project) Note info on how this command exports all the variables, so will need to clean-up the output for the source file.

Next the blog introduces us to ```podman play kube foo.yaml``` which allows us to create and deploy the pod.  Effectivly this command provides the ```docker-compose``` functionality desired, both the yaml container definition and the easy cli start/stop of multiple containers.

Podman being available on Solus will ease the process of generating k8s yaml files.

So created a podman container, it ran, but ```generate kube``` didn't becuase it is looking for a pod.  Read this link, https://developers.redhat.com/blog/2019/01/15/podman-managing-containers-pods/ .  So created a pod and added the container to it, cannot access the container app from browswer (no port binding?).  Generating the yml worked for the auto-named pod, and it contains the container info, did not use ```- s```.

Stopped and removed the container.  Used the ```play kube``` command and new pod and container were successfully created.  Note I didn't remove the pod.  The ```generate``` function removed the _ from the existing pod name, but left it in for HOSTNAME value.  Something to watch, but probably avoidable once I start naming pods.

So here we can do more research on kubernetes yaml and reference it back in.  k8s pod yaml looks simple enough to learn straight forwardly.  Our example so far has shown how environemnt variables are structured.  We need to run a test that also contains lables & annotations(for traefik & other support), and still need to better understand the port mapping structure.

### Podman pod-container configurations

Ok, so lets get our heads around pods & containers. I've been watching k8s videos online, so starting to get my head around it.  We can familiarize ourselves with podman pod commands here, https://docs.podman.io/en/latest/pod.html.  Documentation is light, and tutorials only cover the most basic docker replacement functionality.  But the command reference and (most likely) api documentation is thourough and should provide much valuable info.

So next I start reviewing here, https://developers.redhat.com/blog/2019/01/15/podman-managing-containers-pods/.  
We see interesting functionality in the example of ```podman create --pod``` where you can create a container and it's new pod in 1 command, or add to a seperatly created pod.  Note the ```new:``` prefix in the pod flag.  So lets run a test case. Naming pod and contianer the same creates an error, so autonaming the container in this test case.  I've also added a label to the ```podman run``` statement to see how that gets defined in the yaml file. ```sudo podman run -d --pod new:mkvcleaver -e USER_ID=1000 -e GROUP_ID=1000 -p 5800:5800 -v /home/primary/podman/mkvcleaver:/config:rw -v /home/primary:/storage:rw -e TZ=America/New_York -l traefik.port=80 jlesage/mkvcleaver```

I've got a running pod with 2 containers, and the app container is running with autoname sharp_matsumoto.  Can we reach the app via the browser? Yes.  So, running the command this way auto handled the port mappings between container-pod-host.  That's what we need for the r320 test case, but still leaves things to learn for our production test case with reverse-proxy where we only want to map between container-pod and not out to the host.
So let's generate that yaml file and check it out. ```sudo podman generate kube mkvcleaver  > mkvcleaver_v2.yml``` Success.  
So not seeing the label in the generated yml file.  Is it on the pod-container? We can check with ```podman pod inspect``` and ``` podman container inspect```.  So lets start with the pod.
```podman pod inspect mkvcleaver```  Not seeing it there, nor anything else really container specific like the environmental variables, volumes, etc.  To be expected, but good to check.
```podman container inspect sharp_matsumoto``` Ok, its a big output.  Saved the json output.  The label is there, along with the labels coming from the container image itself.  So they are there if neeeded.  Does traefik for k8s use labels?  Also ```podman pod create -l``` to add labels to the pod.  So may need to investigate pod labels for our k8s testing and production.

### Generate systemd service

In reading through this blog, 
https://www.redhat.com/sysadmin/podman-shareable-systemd-services
, we see demonstrated how easy it is to use ```podman generate systemd --name foo``` to generate the container service file, which can be easily cli & gui started and stopped.  Note that the generated service file uses the command ```podman start foo```.  Not sure if changing that to ```podman play kube foo``` in line with prior section would be better.  Does ```play kube``` recreate the container if an updated image is available?

So now that we have working pod-contianer that successfulle exports its yaml config. Lets see what happens when we try to export the systemd file.  First query, use pod or container name?  Lets see.
Pod: ```podman generate systemd mkvcleaver  > mkvcleaver_v2.service``` No errors, generated a file.  
Container: ```podman generate systemd sharp_matsumoto  > sharp_v2.service``` No errors, generated a file.  
So in looking at the 2 files, both look valid.  First key observation, the pod file is much larger, and in comparing further contains within the entirety of the containers' file.  So contianer has the typical 3 sections of Unit, Service, and Install.  The pod contains each of those twice, so 6 sections. One set for pod and one set for its' container.
Second key observation is that the systemd service names are the podman name hash's. So the intrinsic ```podman run/stop``` commands in the service file reference that hash, not the pod/container name we apply.  So in use case where we replace the container/pod with updated image (and hence new hash name) the service file will no longer be valid.  So the ```play kube``` command in the service file becomes now more important, and ultimately would likely need to be self generated(?). Thinking on that went a checked command reference here https://docs.podman.io/en/latest/markdown/podman-play-kube.1.html.  
Also in searching, found this https://github.com/containers/podman/issues/2752, still need to read and understand, but I'm not the first with this execution strategy.  Looks like he, in resolution, has working service file, just forgot 1 line.
So let's check out his github for possible examples.  https://github.com/ikke-t
We conveneniently find two j2 files as examples in https://github.com/ikke-t/podman-container-systemd/tree/master/templates.  So we're good to go there, but will at least need to look at systemd files for system vs user as part of this.  Which will take us back here
https://www.redhat.com/sysadmin/podman-shareable-systemd-services; where they run their systemd service as user rather than root/system, by placing the file in ~/.config/systemd/user/container.service.  Then running ```systemctl --user start container.service``` to start and ```systemctl --user status container.service``` to check.  Notes proper way to stop the service is ```systemctl --user stop container.service```.

### Ansible

So, I've already successfully written a basic playbook to install podman on an Ubuntu test machine.  Have been improving that and creating roles for it.  Test run coming shortly.
The ikke-t github we were looking at for systemd file structure happens to be a repository for ansible role to setup podman containers run by systemd.  https://github.com/ikke-t/podman-container-systemd  So could automate deployment to the host.  Something there, but may be beyond the use case for the intended host machine.  ALso though the issues contain interesting info on the systemd service files.

### Cockpit

See here, .deb may be on the way. https://github.com/cockpit-project/cockpit-podman/issues/481

They don't think the cockpit part will be hard to get into Ubuntu, but the podman part.  Someone tested and the kubic version isn't built correctly to work with cockpit due to how it was packaged.

### Other Notes

Podman’s local repository is in ```/var/lib/containers```
Podman rootless port cannot expose privileged ports less than 1024, so will need to look into that for proxy container.

## Traefik

Support is coming:
https://github.com/containous/traefik/issues/5730

## Synopsis

Think I've got the jist down and could proceed as planned.  A few items to be worked out but no show stoppers.  Getting traefik working and podman source repository are the only key points in my deployments.  

After having stepped away to work on my microk8s project my thinking on podman use has changed a bit.  So here is how I currently see podman use going forward.  For the storage server use case it would be just almost as easy to use microk8s, and microk8s being a snap has the advantage of a direct installation method, versus adding a ppa to install podman and the potential issues/constraints that brings.  On my Solus laptop, going to switch my container work (image testing or sporadic workload driven image use) from docker to podman.  Will remove docker.  Podman is native install.  Podman use allows me work with the new technology, leverage cgroups, and leverage podman-k8s integrations.

## Project status

Stagnant - on hold ; moved on to microk8s.  Will revisit in Ubuntu 21.10.

